﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nasledovanie
{
    class Warrior:Unit
    {
        private double buff;

        public Warrior(int x, int y, double hp, double damage, double buff) : base('W', x, y, hp, damage, "Warrior")
        {
            this.buff = buff;
        }

        public override string GetInfoInString()
        {
            return base.GetInfoInString() + $" Buff:{buff}";
        }

        public override void Attack(Unit unit)
        {
            Console.WriteLine("Warrior attack");
        }
    }
}
