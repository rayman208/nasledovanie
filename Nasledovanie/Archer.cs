﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nasledovanie
{
    class Archer : Unit
    {
        private int countArrows;
        private double accuracy;

        public Archer(int x, int y, double hp, double damage, int countArrows, double accuracy) : base('A', x, y, hp, damage, "Archer")
        {
            this.countArrows = countArrows;
            this.accuracy = accuracy;
        }

        public override string GetInfoInString()
        {
            return base.GetInfoInString() + $" Count Arrows:{countArrows} Accuracy:{accuracy}";
        }

        public override void Attack(Unit unit)
        {
            Console.WriteLine("Archer attack");
        }
    }
}
