﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nasledovanie
{
    abstract class Unit
    {
        private char view;
        private int x, y;
        private double hp;
        private double damage;
        private string name;

        protected Unit(char view, int x, int y, double hp, double damage, string name)
        {
            this.view = view;
            this.x = x;
            this.y = y;
            this.hp = hp;
            this.damage = damage;
            this.name = name;
        }

        public void Move(int dx, int dy)
        {
            x += dx;
            y += dy;
        }


        public virtual string GetInfoInString()
        {
            return $"Name:{name} HP:{hp} Damage:{damage} X:{x} Y:{y}";
        }

        public abstract void Attack(Unit unit);
    }
}
