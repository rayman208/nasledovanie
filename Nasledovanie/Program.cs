﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nasledovanie
{
    class Program
    {
        static void Main(string[] args)
        {
            //Unit u = new Unit('d',1,1,3,4,"ddd");
            Archer a = new Archer(1,1,12,43,5,3.4);
            Warrior w = new Warrior(1,3,4,5,6);

            Console.WriteLine(a.GetInfoInString());
            Console.WriteLine(w.GetInfoInString());

            Console.WriteLine("----------");

            Unit[] units = new Unit[2];
            units[0] = new Archer(1, 1, 12, 43, 5, 3.4);
            units[1] = new Warrior(1, 3, 4, 5, 6);

            for (int i = 0; i < units.Length; i++)
            {
                Console.WriteLine(units[i].GetInfoInString());
                units[i].Attack(a);

                /*if (units[i] is Archer)
                {
                    Console.WriteLine((units[i] as Archer).GetInfoInString());
                    //(units[i] as Archer).GetInfoInString();
                    //((Archer) units[i]).GetInfoInString();
                }else if (units[i] is Warrior)
                {
                    Console.WriteLine((units[i] as Warrior).GetInfoInString());
                }*/
            }


            Console.ReadKey();
        }
    }
}
